# tidu-study

## 编写规范

* [编写规范](./规范/编写规范.md)



# Java基础

* [HashMap（杨柳第一周）](./java基础/HashMap源码分析.md)
* [LinkedList（杨柳第一周）](./java基础/LinkedList.md)
* [ArrayList （胡凯斌第一周）](./java基础/ArrayList源码分析.md)
* WeakHashMap （胡凯斌第一周）
* PriorityQueue
* Stack & Queue 

## 多线程与并发

* ThreadLocal（胡缘第二周）
* ThreadPoolExecutor & Executors (胡缘第二周)
* 

## 运维与诊断

* Arthas（胡凯斌第二周，需要进行相关案例说明，找时间内部培训）
* Mat
* Ansible

## Mysql

* 死锁问题诊断与分析（胡缘第一周）
* [索引（胡缘第一周）](./mysql/MySql索引的底层实现原理.md)

## 设计模式

* 策略模式(杨柳第二周)
* 模板模式（杨柳第二周）
* 
