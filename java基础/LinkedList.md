

#  概述

LinkedList底层使用了双向链表保存数据，LinkedList是有序的，线程不安全。

# 结构

![image-20220729095539747](../img/image-20220729095539747.png)

# 源码分析

**相关源码都是基于JDK8进行分析。**

关键属性

```
    // 大小
    transient int size = 0;

    /**
     * Pointer to first node.
     * Invariant: (first == null && last == null) ||
     *            (first.prev == null && first.item != null)
     * 指向第一个节点
     /
    transient Node<E> first;

    /**
     * Pointer to last node.
     * Invariant: (first == null && last == null) ||
     *            (last.next == null && last.item != null)
     * 指向尾节点
     */
    transient Node<E> last;

```

add方法源码解析:

```java
 public boolean add(E e) {
        linkLast(e);
        return true;
    }

void linkLast(E e) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, e, null);
        last = newNode;
        // 在LinkedList的末尾插入元素，因为有last指向链表末尾。
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
        modCount++;
    }

```

get方法会从头节点向下遍历，直到找到对应的数据：

```java
 public E get(int index) {
        checkElementIndex(index);
        return node(index).item;
    }
```

# 总结

* LinkedList不是线程安全的,可以使用`Collections.synchronizedList()`保证线程安全
* LinkedList底层使用双向链表存储数据，添加删除快，复杂度为o(1) ,查询最大复杂度0(n)。
* LinkedList是有序的，适合在某些需要保证数据顺序性的场景下使用。