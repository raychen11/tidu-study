

# ArrayList笔记

#  概述

ArrayList是它继承于 **AbstractList**，实现了 **List**, **RandomAccess**, **Cloneable**, **java.io.Serializable** 这些接口。底层是一个动态数组，同时也是我们经常使用的集合，他允许任何对象的插入，甚至包括null，同时它的初始容量为10，该容量代表了数组的大小，但在添加大量元素前，应用程序可以使用ensureCapacity 操作来增加 ArrayList 实例的容量。这可以减少递增式再分配的数量。

# 结构

ArrayList的数据结构是动态数组

ArrayList初始化机制的逻辑如下：

* 在第一次new ArrayList()的时候，会创建初始化一个空的对象到堆中
* 当该对象第一次调用add方法时，底层会调用calculateCapacity方法，传入当前元素，和当前add操作需要的容量
* 如果是第一次进入的时候，会进行一次判断，如果符合这个判断，取初始化容量（10）和 当前add操作需要的容量的最大值，并返回
* 随后再调用ensureExplicitCapacity方法，调用该方法，为了避免多线程问题会对当前的add操作做一个计数；
* 随后判断返回的参数是否大于当前容量大小，如果大于则调用grow方法
* 第一次添加在grow方法中，会将之前返回的最大值赋值给元素，并返回容量为10的空数组
* 再回到add方法，会将值赋值给位置为0的数组上，并进行size++，随后返回;

ArrayList扩容机制的逻辑如下：

* 首先会调用calculateCapacity校验是否是第一次进入，如果不是则直接跳过判断返回当前add操作需要的容量

* 随后再调用ensureExplicitCapacity方法，调用该方法，为了避免多线程问题会对当前的add操作做一个计数；

* 随后判断返回的参数是否大于当前容量大小，如果大于则调用grow方法

* 随后将容量当前容量复制给oldCapacity变量，然后将oldCapacity扩容1.5倍赋值给newCapacity变量

* 随后调用Arrays.copyOf（）方法在不破坏原有数据的前提，给原数组容量扩容；

* 再回到add方法，会将值赋值给位置为10的数组上，并进行size++，随后返回;

  

# 源码分析

**相关源码都是基于JDK8进行分析。**

关键属性

```
    // 默认容量
    private static final int DEFAULT_CAPACITY = 10;
    
    // 校验是否是初始化
    private static int calculateCapacity(Object[] elementData, int minCapacity) {
        if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
            return Math.max(DEFAULT_CAPACITY, minCapacity);
        }
        return minCapacity;
    }

	// 校验是否需要扩容
    private void ensureExplicitCapacity(int minCapacity) {
        modCount++;

        // overflow-conscious code
        if (minCapacity - elementData.length > 0)
            grow(minCapacity);
    }

	// 扩容机制
    private void grow(int minCapacity) {
        // 将当前容器大小赋值给oldCapacity变量
        int oldCapacity = elementData.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;
        if (newCapacity - MAX_ARRAY_SIZE > 0)
            newCapacity = hugeCapacity(minCapacity);
        // 最小容量通常接近大小，因此这是一个成功之处:
        elementData = Arrays.copyOf(elementData, newCapacity);
    }

```



# 总结

**ArrayList具有以下特点**

- 允许插入不同类型的元素包括重复的元素
- 插入的元素是有序的
- 是动态扩容的
- 线程不安全(因为每一次添加的时候计数的时候是采用i++形式的会出现指令重排问题)
- 基于动态数组的数据结构

​	
